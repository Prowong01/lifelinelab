import { useDisclosure } from "@mantine/hooks";
import { Card, Image, Text, Button, Modal } from "@mantine/core";

import FriendModal from "./FriendModal";
import { Friend } from "../types/Friend";

import classes from "../../styles/CardWithStats.module.css";

interface FriendCardProps {
  friend: Friend;
}

const FriendCard: React.FC<FriendCardProps> = ({ friend }) => {
  const [opened, { open, close }] = useDisclosure(false);

  return (
    <>
      <Modal opened={opened} onClose={close}>
        <FriendModal friend={friend} />
      </Modal>

      <Card
        withBorder
        padding="lg"
        radius="md"
        className={classes.card}
        style={{ width: "200px" }}
      >
        <Card.Section>
          <Image
            style={{ margin: "0 auto" }}
            src={friend.picture.large}
            height={125}
            w={125}
            alt={`${friend.name.first} ${friend.name.last}`}
          />
        </Card.Section>

        <Card.Section>
          <h4
            style={{
              display: "flex",
              justifyContent: "center",
              whiteSpace: "nowrap",
            }}
          >
            First Name:{` ${friend.name.first}`}
          </h4>
          <h4
            style={{
              display: "flex",
              justifyContent: "center",
              whiteSpace: "nowrap",
            }}
          >
            Last Name:{` ${friend.name.last}`}
          </h4>
        </Card.Section>

        <Card.Section className={classes.footer}>
          <Text fz="xs" style={{ margin: "5px 0", whiteSpace: "nowrap" }}>
            Email:
            <div>{friend.email}</div>
          </Text>
          <Text fz="xs" style={{ margin: "5px 0", whiteSpace: "nowrap" }}>
            Phone Number:
            <div>{friend.phone}</div>
          </Text>
        </Card.Section>

        <Button
          color="blue"
          fullWidth
          mt="md"
          radius="md"
          onClick={open}
          style={{ display: "block" }}
        >
          Show Modal
        </Button>
      </Card>
    </>
  );
};

export default FriendCard;
