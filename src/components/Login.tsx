import React, { useState } from "react";
import axios from "axios";
// import CryptoJS from "crypto-js";

import { useForm } from "@mantine/form";
import {
  TextInput,
  PasswordInput,
  Text,
  Paper,
  Stack,
  Button,
} from "@mantine/core";
import { useNavigate } from "react-router-dom";

interface LoginProps {
  onLogin: () => void;
}

const Login: React.FC<LoginProps> = ({ onLogin }) => {
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const form = useForm({
    initialValues: {
      username: "",
      password: "",
      terms: true,
    },
  });

  const handleLogin = async () => {
    const { username, password } = form.values;
    try {
      const response = await axios.get("https://randomuser.me/api/?seed=lll");
      const users = response.data.results;

      let userFound = false;

      for (const user of users) {
        // const hashedPassword = CryptoJS.SHA256(
        //   password
        // ).toString();
        // console.log(hashedPassword);
        if (
          user.login.username === username &&
          user.login.password === password
        ) {
          userFound = true;
          break;
        }
      }

      if (userFound) {
        onLogin();
        navigate("/friends");
      } else {
        setError("Invalid username or password");
      }
    } catch (error) {
      setError("An error occurred during login");
    }
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <Paper
        radius="md"
        p="xl"
        withBorder
        style={{ width: "100%", maxWidth: 400 }}
      >
        <Text size="lg">Welcome to LifeLineLab</Text>
        <form onSubmit={form.onSubmit(handleLogin)}>
          <Stack>
            <TextInput
              required
              label="Username"
              placeholder="Your username"
              value={form.values.username}
              onChange={(event) =>
                form.setFieldValue("username", event.currentTarget.value)
              }
              error={form.errors.username && "Invalid username"}
              radius="md"
            />
            <PasswordInput
              required
              label="Password"
              placeholder="Your password"
              value={form.values.password}
              onChange={(event) =>
                form.setFieldValue("password", event.currentTarget.value)
              }
              error={
                form.errors.password &&
                "Password should include at least 6 characters"
              }
              radius="md"
            />
            <Button type="submit">Login</Button>
          </Stack>
          {error && <Text color="red">{error}</Text>}
        </form>
      </Paper>
    </div>
  );
};

export default Login;
