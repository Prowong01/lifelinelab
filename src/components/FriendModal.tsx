import React from "react";
import { Friend } from "../types/Friend";
import { Image } from "@mantine/core";

interface FriendModalProps {
  friend: Friend;
}

const FriendModal: React.FC<FriendModalProps> = ({ friend }) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          justifyItems: "center",
        }}
      >
        <Image
          src={friend.picture.large}
          alt={`${friend.name.first} ${friend.name.last}`}
          height={125}
          w={125}
        />
      </div>
      <div>
        <p>First Name: {friend.name.first}</p>
        <p>Last Name: {friend.name.last}</p>
        <p>Email: {friend.email}</p>
        <p>Phone: {friend.phone}</p>
        <p>D.O.B: {new Date(friend.dob.date).toLocaleDateString()}</p>
        <p>
          Address:{" "}
          {`${friend.location.street.name}, ${friend.location.city}, ${friend.location.state}, ${friend.location.country}`}
        </p>
      </div>
    </div>
  );
};

export default FriendModal;
