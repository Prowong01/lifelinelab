import React, { useState, useEffect } from "react";
import axios from "axios";

import FriendCard from "./FriendCard";
import { Friend } from "../types/Friend";
import { Button, Container, Pagination, TextInput } from "@mantine/core";

const FriendsList: React.FC = () => {
  const [friends, setFriends] = useState<Friend[]>([]);
  const [activePage, setActivePage] = useState(1);
  const friendsPerPage = 25;
  const [pageInput, setPageInput] = useState(activePage.toString());
  const [error, setError] = useState<string | null>(null);

  const fetchFriends = async (page: number) => {
    try {
      const response = await axios.get(
        `https://randomuser.me/api/?seed=lll&page=${page}&results=${friendsPerPage}`
      );
      setFriends(response.data.results);
    } catch (error) {
      console.error("Failed to fetch friends:", error);
    }
  };

  useEffect(() => {
    fetchFriends(activePage);
  }, [activePage]);

  useEffect(() => {
    setPageInput(activePage.toString());
  }, [activePage]);

  const handlePageInputChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setPageInput(event.target.value);
  };

  const handlePageInputBlur = () => {
    let pageNumber = parseInt(pageInput, 20);
    if (isNaN(pageNumber) || pageNumber < 1 || pageNumber > 20) {
      setError(`Please enter a number between 1 and 20.`);
      pageNumber = Math.max(1, Math.min(20, pageNumber));
    } else {
      setError(null);
    }
    setActivePage(pageNumber);
  };

  const handlePageInputKeyPress = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (event.key === "Enter") {
      handlePageInputBlur();
    }
  };

  return (
    <Container
      fluid
      size="responsive"
      pt={12}
      pb={14}
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        maxWidth: "1200px",
      }}
    >
      <div
        style={{
          display: "flex",
          width: "1100px",
          justifyContent: "space-between",
          alignItems: "center",
          marginBottom: "20px",
        }}
      >
        <h2
          style={{
            fontSize: "20px",
            textAlign: "center",
          }}
        >
          Friend List
        </h2>
        <div style={{ display: "flex", width: "200px" }}>
          <TextInput
            value={pageInput}
            onChange={handlePageInputChange}
            onBlur={handlePageInputBlur}
            onKeyPress={handlePageInputKeyPress}
            style={{ width: "60px" }}
            error={error}
          />

          <Button onClick={handlePageInputBlur}>Go</Button>
        </div>
      </div>

      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          gap: "20px",
          maxWidth: "1100px",
        }}
      >
        {friends.map((friend, index) => (
          <FriendCard key={index} friend={friend} />
        ))}
      </div>
      <Pagination
        total={10}
        value={activePage}
        onChange={setActivePage}
        mt="sm"
        style={{ margin: "0 auto" }}
      />
    </Container>
  );
};

export default FriendsList;
