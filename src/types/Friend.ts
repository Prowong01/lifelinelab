export interface Friend {
    name: {
    title: string;
      first: string;
      last: string;
    };
    email: string;
    phone: string;
    picture: {
      thumbnail: string;
      large: string;
    };
    dob: {
      date: string;
    };
    location: {
      street: {
        name: string;
      };
      city: string;
      state: string;
      country: string;
    };
  }
  