import React from "react";
import { MantineProvider } from "@mantine/core";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <MantineProvider
      theme={{
        headings: {
          fontFamily: "Graduate, slab-serif",
        },
        primaryColor: "indigo",
        primaryShade: { light: 6, dark: 4 },
        breakpoints: {
          xs: "30em",
          sm: "48em",
          md: "64em",
          lg: "74em",
          xl: "90em",
        },
      }}
    >
      <App />
    </MantineProvider>
  </React.StrictMode>
);
